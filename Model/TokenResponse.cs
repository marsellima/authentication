﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authentication.Shared
{
    public class TokenResponse
    {
        public string Token { get; set; }
    }
}