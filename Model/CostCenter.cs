﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authentication.Shared
{
    public class CostCenter
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}