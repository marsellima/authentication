﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authentication.Shared
{
    public class TokenRequest
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}