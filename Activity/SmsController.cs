﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Authentication.Activities.Main;
using Authentication.Shared;
using Android.Telephony;


namespace Authentication.Activity
{
    [Activity(Label = "Authentication", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : ListActivity
    {
        private SmsSentReceiver receiver;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.activity_main);
            EditText editaTexto = FindViewById<EditText>(Resource.Id.editaTexto);
            EditText foneTexto = FindViewById<EditText>(Resource.Id.foneTexto);
            Button enviaSms = FindViewById<Button>(Resource.Id.enviaSms);

            if (!PackageManager.HasSystemFeature(Android.Content.PM.PackageManager.FeatureTelephony))
            {

                enviaSms.Click += delegate
                {
                    var sentIntent = new Intent(SmsSentReceiver.SentIntent);
                    var sent = PendingIntent.GetBroadcast(this, 0, sentIntent, 0);
                    var deliveredIntent = new Intent(SmsSentReceiver.DeliveredIntent);
                    var delivered = PendingIntent.GetBroadcast(this, 0, deliveredIntent, 0);

                    var manager = SmsManager.Default;
                    manager.SendTextMessage(foneTexto.Text, null, editaTexto.Text, sent, delivered);
                };
            }
        }
        protected override void OnResume()
        {
            base.OnResume();
            receiver = new SmsSentReceiver();
            RegisterReceiver(receiver, new IntentFilter(SmsSentReceiver.SentIntent));
            RegisterReceiver(receiver, new IntentFilter(SmsSentReceiver.DeliveredIntent));
        }

        protected override void OnPause()
        {
            base.OnPause();
            UnregisterReceiver(receiver);
        }

        private class SmsSentReceiver : BroadcastReceiver
        {
            public const string SentIntent = "SentSMS";
            public const string DeliveredIntent = "DeliveredSMS";

            public override void OnReceive(Context context, Intent intent)
            {
                if (ResultCode == Result.Ok)
                {
                    if (intent.Action == SentIntent)
                    {
                        Toast.MakeText(context, "SMS Enviado.", ToastLength.Short).Show();
                    }
                    else if (intent.Action == DeliveredIntent)
                    {
                        Toast.MakeText(context, "SMS despachado.", ToastLength.Short).Show();
                    }
                }
                else
                {
                    if (intent.Action == SentIntent)
                    {
                        Toast.MakeText(context, "Falha ao enviar SMS.", ToastLength.Short).Show();
                    }
                    else if (intent.Action == DeliveredIntent)
                    {
                        Toast.MakeText(context, "Falha ao encaminhar SMS.", ToastLength.Short).Show();
                    }
                }
            }
        }
    }
}

