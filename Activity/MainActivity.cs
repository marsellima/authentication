﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Authentication.Activities.Main;
using Authentication.Shared;

namespace Authentication
{
    [Activity(Label = "Authentiation")]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            SetContentView(Resource.Layout.activity_main);

            Configure();
        }

        private void Configure()
        {
        }

        private void ShowDialogConfiguration()
        {
            var dialog = new Android.App.AlertDialog.Builder(this);

            var view = LayoutInflater.Inflate(Resource.Layout.dialog_configure_credentials_title, null, false);

            dialog.SetTitle(Resource.String.dialog_configure_credential_title);

            dialog.SetView(view);

            dialog.SetPositiveButton(Resource.String.confirm, (sender, e) =>
            {

            });

            dialog.Show();
        }
    }
}