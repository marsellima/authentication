﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using RestSharp;
using System.Net.WebSockets;
using System.Threading;

namespace Authentication.Shared
{
    public class ApiResponse<T>
    {
        public T Dados { get; set; }
    }


    public class ApiController
    {
        private RestClient Rest { get; set; }
        private string _host;
        private string _token;

        private ApiResponse<T> ProcessarRespostaRest<T>(IRestResponse<T> restResponse)
        {
            return new ApiResponse<T>
            {
                Dados = restResponse.Data
            };
        }

     
        public string GetToken()
        {
            var authenticationUrl = "https://rssolutions.sender.cloud/api/v1/token";
            Rest = new RestClient(authenticationUrl);

            var request = new RestRequest(authenticationUrl, Method.POST);

            var tokenRequest = new TokenRequest
            {
                username = "rssolutions-api",
                password = "rs789456rs"
            };  

            request.AddJsonBody(tokenRequest);

            return ProcessarRespostaRest(Rest.Execute<TokenResponse>(request)).Dados.Token;
        }

       
    }
}