﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Authentication.Adapters;
using Authentication.Shared;
using Authentication;
using CheeseBind;
using RestSharp;

namespace Authentication.Activities.Main
{
    [Activity(Label = "CostCenterActivity", MainLauncher = true)]
    public class CostCenterActivity : AppCompatActivity
    {
      
        private CostCenterAdapter _costCenterAdapter;

        [BindView(Resource.Id.rvw_items)] RecyclerView rvwCostCenterList;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            SetContentView(Resource.Layout.activity_cost_center);

            Cheeseknife.Bind(this);

            Configure();

            var requisicao = new ApiController().GetToken();

            var sunda = requisicao;
        }

        private void Configure()
        {
            InitializeCostCenterList();
        }

        private void InitializeCostCenterList()
        {
            var costCenterList = new List<CostCenter>();


           
            var client = new RestClient("https://rssolutions.sender.cloud/api/v1/companies/5HeCWZ4jFCoKpAg2v/costCenters");

            var request = new RestRequest(Method.GET);

            request.AddJsonBody(costCenterList);
            IRestResponse response = client.Execute(request);
            
            
            _costCenterAdapter = new CostCenterAdapter(costCenterList);
            rvwCostCenterList.SetAdapter(_costCenterAdapter);
            rvwCostCenterList.SetLayoutManager(new LinearLayoutManager(this));
        }

    }
}