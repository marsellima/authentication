﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Authentication.Shared;
using CheeseBind;

namespace Authentication.Adapters
{
    public class CostCenterAdapter : RecyclerView.Adapter
    {
        public override int ItemCount => _costCenterList.Count();

        private readonly List<CostCenter> _costCenterList = new List<CostCenter>();

        public CostCenterAdapter(List<CostCenter> costCenterList)
        {
            _costCenterList = costCenterList;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            (holder as ViewHolder)?.Bind(_costCenterList[position]);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            return new ViewHolder(LayoutInflater.From(parent.Context).Inflate(Resource.Layout.list_item_cost_center, parent, false));
        }

        public class ViewHolder : RecyclerView.ViewHolder, View.IOnClickListener
        {
            [BindView(Resource.Id.tvw_item_description)] TextView _description;

            public ViewHolder(View itemView) : base(itemView)
            {
                Cheeseknife.Bind(this, itemView);
            }

            public void Bind(CostCenter costCenter)
            {
                _description.Text = costCenter.Name;

                ItemView.SetOnClickListener(this);
            }

            public void OnClick(View v)
            {
            }
        }
    }
}